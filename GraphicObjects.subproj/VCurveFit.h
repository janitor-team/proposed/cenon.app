/* VCurveFit.h - Objective-C Frontend for curve fitting code
 *
 * Copyright (C) 2011 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  2011-04-05
 * modified: 2011-04-05
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_VCURVEFIT
#define CEN_H_VCURVEFIT

#include "VPath.h"

@interface VCurveFit:NSObject
{
    NSMutableArray  *cList; // list with created graphic objects
}

+ (VCurveFit*)sharedInstance;
- (VPath*)fitGraphic:(VGraphic*)g maxError:(double)maxError;    // fit this Path or PolyLine
- (NSMutableArray*)list;                                        // get created graphics elements

- (void)dealloc;

@end

#endif // CEN_H_VCURVEFIT
