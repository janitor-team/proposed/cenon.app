/* VLine3D.h
 * 3-D line object
 *
 * Copyright (C) 1996-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  1996-01-19
 * modified: 2011-04-04 (-line3DWithPoints: added)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_VLINE3D
#define CEN_H_VLINE3D

#include "VLine.h"

#define  PTS_LINE	2

@interface VLine3D: VLine
{
    float	z0, z1;		// the z level of the line (0 - 1)
}

/* class methods */
+ (VLine3D*)line3D;
+ (VLine3D*)line3DWithPoints:(V3Point)p0 :(V3Point)p1;

/* line methods */
- (void)setVertices3D:(V3Point)pv0 :(V3Point)pv1;
- (void)setZLevel:(float)zv0 :(float)zv1;
- (void)getZLevel:(float*)zv0 :(float*)zv1;

@end

#endif // CEN_H_VLINE3D
