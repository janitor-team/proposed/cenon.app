/* VHFArrayAdditions.h
 * NSArray + NSMutableArray Additions
 *
 * Copyright (C) 1999-2016 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  1999-06-11
 * modified: 2016-02-17
 *
 * This file is part of the Cenon Shared Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by the Cenon GmbH. Among other things,
 * the License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this library; see the file LICENSE. If not, write to Cenon.
 *
 * If you want to link this library to your proprietary software,
 * or for other uses which are not covered by the definitions
 * laid down in the Cenon Public License, vhf also offers a proprietary
 * license scheme. See the vhf internet pages or ask for details.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.cenon.info
 */

#ifndef CEN_H_ARRAYADDITIONS
#define CEN_H_ARRAYADDITIONS

#include <Foundation/Foundation.h>

@interface NSArray(VHFArrayAdditions)
- (int)intAtIndex:(int)ix;
- (float)floatAtIndex:(int)ix;
- (double)doubleAtIndex:(int)ix;

- (NSUInteger)indexOfCaseInsensitiveString:(NSString*)aString;
@end

@interface NSMutableArray(VHFArrayAdditions)
- (void)addInt:(int)i;
- (void)addDouble:(double)f;
@end

#endif // CEN_H_ARRAYADDITIONS
