/*
 * VHFSystemAdditions.m
 *
 * Copyright (C) 2000-2015 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  2000-04-30
 * modified: 2015-03-15 (-fileAttributesAtPath:traverseLink:error: added)
 *           2014-11-07 (NSFileManager Additions added)
 *
 * This file is part of the Cenon Shared Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by the Cenon GmbH. Among other things,
 * the License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this library; see the file LICENSE. If not, write to Cenon.
 *
 * If you want to link this library to your proprietary software,
 * or for other uses which are not covered by the definitions
 * laid down in the Cenon Public License, vhf also offers a proprietary
 * license scheme. See the vhf internet pages or ask for details.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include "VHFSystemAdditions.h"

@implementation NSBundle(VHFBundleAdditions)

/* created:  30.04.00
 * modified:
 */
+ (BOOL)loadModelNamed:(NSString*)fileName owner:(id)owner
{
//#ifdef __linux__
//    return [NSBundle loadGModelNamed:fileName owner:owner];
//#else
    return [NSBundle loadNibNamed:fileName owner:owner];
//#endif
}

@end


@implementation NSFileManager(CenFileManagerAdditions)

/* replacement for -createDirectoryAtPath:attributes:
 *
 * created:  2014-11-07
 * modified:
 *
 * MAC_OS_X_VERSION_MIN_REQUIRED    Deployment target
 * MAC_OS_X_VERSION_MAX_ALLOWED     Base SDK
 */
- (BOOL)createDirectoryAtPath:(NSString*)path recursive:(BOOL)recursive
                   attributes:(NSDictionary*)attributes error:(NSError **)error
{
#   if MAC_OS_X_VERSION_MIN_REQUIRED < 1050 /*MAC_OS_X_VERSION_10_5*/           // deployment target < 10.5
    if ( ! [self respondsToSelector:@selector(createDirectoryAtPath:withIntermediateDirectories:attributes:error:)] )
        return [self createDirectoryAtPath:path attributes:attributes];         // <= 10.4
#   endif                                                                       // >= 10.5
        return [self createDirectoryAtPath:(NSString*)path withIntermediateDirectories:(BOOL)recursive
                                attributes:(NSDictionary*)attributes error:(NSError **)error];
}

/* created:  2014-11-07
 * modified:
 */
- (BOOL)removeFileAtPath:(NSString*)path error:(NSError **)error
{
#   if MAC_OS_X_VERSION_MIN_REQUIRED < 1050 /*MAC_OS_X_VERSION_10_5*/           // deployment target < 10.5
    if ( ! [self respondsToSelector:@selector(moveItemAtPath:toPath:error:)] )  // <= 10.4
        return [self removeFileAtPath:path handler:nil];
#   endif                                                                       // >= 10.5
        return [self removeItemAtPath:path error:error];
}

/* created:  2014-11-07
 * modified:
 */
- (BOOL)movePath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)error
{
#   if MAC_OS_X_VERSION_MIN_REQUIRED < 1050 /*MAC_OS_X_VERSION_10_5*/           // deployment target < 10.5
    if ( ! [self respondsToSelector:@selector(moveItemAtPath:toPath:error:)] )  // <= 10.4
        return [self movePath:srcPath toPath:dstPath handler:nil];
#   endif                                                                       // >= 10.5
        return [self moveItemAtPath:srcPath toPath:dstPath error:error];
}

/* created:  2014-11-07
 * modified:
 */
- (BOOL)copyPath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)error
{
#   if MAC_OS_X_VERSION_MIN_REQUIRED < 1050 /*MAC_OS_X_VERSION_10_5*/           // deployment target < 10.5
    if ( ! [self respondsToSelector:@selector(moveItemAtPath:toPath:error:)] )  // <= 10.4
        return [self copyPath:srcPath toPath:dstPath handler:nil];
#   endif                                                                       // >= 10.5
    return [self copyItemAtPath:srcPath toPath:dstPath error:error];
}

/* created:  2014-11-07
 * modified:
 */
- (NSArray*)directoryContentsAtPath:(NSString *)path error:(NSError **)error
{
#   if MAC_OS_X_VERSION_MIN_REQUIRED < 1050 /*MAC_OS_X_VERSION_10_5*/               // deployment target < 10.5
    if ( ! [self respondsToSelector:@selector(contentsOfDirectoryAtPath:error:)] )  // <= 10.4
        return [self directoryContentsAtPath:path handler:nil];
#   endif                                                                           // >= 10.5
    return [self contentsOfDirectoryAtPath:path error:error];
}
- (NSArray*)contentsOfDirectoryAtPath:(NSString *)path
{
#   if MAC_OS_X_VERSION_MIN_REQUIRED < 1050 /*MAC_OS_X_VERSION_10_5*/               // deployment target < 10.5
    if ( ! [self respondsToSelector:@selector(contentsOfDirectoryAtPath:error:)] )  // <= 10.4
        return [self directoryContentsAtPath:path handler:nil];
#   endif                                                                           // >= 10.5
    return [self contentsOfDirectoryAtPath:path error:NULL];
}

/* replacement for -fileAttributesAtPath:traverseLink:
 *
 * created:  2014-11-16
 * modified:
 *
 * MAC_OS_X_VERSION_MIN_REQUIRED    Deployment target
 * MAC_OS_X_VERSION_MAX_ALLOWED     Base SDK
 */
- (NSDictionary*)fileAttributesAtPath:(NSString*)path traverseLink:(BOOL)traverse error:(NSError **)error
{
#   if MAC_OS_X_VERSION_MIN_REQUIRED < 1050 /*MAC_OS_X_VERSION_10_5*/           // deployment target < 10.5
    if ( ! [self respondsToSelector:@selector(attributesOfItemAtPath:error:)] )
        return [self fileAttributesAtPath:path traverseLink:traverse];          // <= 10.4
#   endif                                                                       // >= 10.5
    return [self attributesOfItemAtPath:path error:error];
}

@end
