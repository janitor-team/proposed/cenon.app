/* VHFPopUpButtonAdditions.h
 * NSPopUpButton additions
 *
 * Copyright (C) 1997-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  1997-10-24
 * modified: 2014-07-06 (selectItemWithTag removed)
 *
 * This file is part of the Cenon Shared Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by the Cenon GmbH. Among other things,
 * the License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this library; see the file LICENSE. If not, write to Cenon.
 *
 * If you want to link this library to your proprietary software,
 * or for other uses which are not covered by the definitions
 * laid down in the Cenon Public License, vhf also offers a proprietary
 * license scheme. See the vhf internet pages or ask for details.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.cenon.info
 */

#ifndef CEN_H_POPUPADDITIONS
#define CEN_H_POPUPADDITIONS

#include <AppKit/AppKit.h>
#include "vhfCompatibility.h"     // NSInteger for OS X 10.4

@interface NSPopUpButton(VHFPopUpButtonAdditions)
- (NSMenuItem*)itemWithTag:(NSInteger)tag;
//- (BOOL)selectItemWithTag:(NSInteger)tag;   // method available since Mac OS X >= 10.4

- (void)replaceItemsFromArray:(NSArray*)items fromIndex:(NSInteger)removeIx;
@end

#endif // CEN_H_POPUPADDITIONS
