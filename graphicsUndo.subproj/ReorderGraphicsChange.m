/* ReorderGraphicsChange.m
 *
 * Copyright (C) 1993-2014 by Cenon GmbH
 * Authors:  Georg Fleischmann
 *
 * created:  1993
 * modified: 2002-07-15
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include "undo.h"

@interface ReorderGraphicsChange(PrivateMethods)

- (void)undoDetails;

@end

@implementation ReorderGraphicsChange

- (void)saveBeforeChange
{
    [super saveBeforeChange];
    [changeDetails makeObjectsPerformSelector:@selector(recordGraphicPositionIn:) withObject:[graphicView layerList]]; 
}

- (Class)changeDetailClass
{
    return [OrderChangeDetail class];
}

- (void)undoDetails
{   OrderChangeDetail   *detail = nil;
    VGraphic            *graphic;
    NSArray             *layList = [graphicView layerList];
    int                 count, i, l, sCnt = [layList count];

    count = [changeDetails count];
    for (i = 0; i < count; i++)
    {
	detail = [changeDetails objectAtIndex:i];
	graphic = [detail graphic];
        for ( l=0; l<sCnt; l++ )
        {   LayerObject	*layerObject = [layList objectAtIndex:l];

            if ( [[layerObject list] containsObject:graphic] )
            {   [layerObject removeObject:graphic];
                [detail setLayer:l];
            }
        }
    }
    for (i = 0; i < count; i++)
    {   LayerObject	*layerObject = [layList objectAtIndex:[detail layer]];

        detail = [changeDetails objectAtIndex:i];
        graphic = [detail graphic];
        // graphic position can be larger than list count because slaylist is sorted different (with selection)
        [layerObject insertObject:graphic atIndex:Min([detail graphicPosition], [[layerObject list] count])];
    }
}

@end
