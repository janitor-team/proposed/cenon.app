/* ColorGraphicsChange.m
 *
 * Copyright (C) 1993-2014 by Cenon GmbH
 * Authors:  Georg Fleischmann
 *
 * created:  1993
 * modified: 2002-07-15
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include "undo.h"

@interface ColorGraphicsChange(PrivateMethods)

- (BOOL)subsumeIdenticalChange:change;

@end

@implementation ColorGraphicsChange

- initGraphicView:aGraphicView color:(NSColor*)aColor colorNum:(int)colNum
{
    [super initGraphicView:aGraphicView];
    color = [aColor copyWithZone:(NSZone*)[self zone]];
    colorNum = colNum;
    return self;
}

- (NSString *)changeName
{
    return COLOR_OP;
}

- (Class)changeDetailClass
{
    return [ColorChangeDetail class];
}

- (NSColor *)color
{
    return color;
}

- (int)colorNum
{
    return colorNum;
}

- (BOOL)subsumeIdenticalChange:change
{   ColorGraphicsChange	*colorChange = (ColorGraphicsChange*)change;

    if ( colorChange->colorNum != colorNum )
        return NO;
    color = [(ColorGraphicsChange *)change color];
    return YES;
}

- (void)dealloc
{
    [color release];
    [super dealloc];
}

@end
