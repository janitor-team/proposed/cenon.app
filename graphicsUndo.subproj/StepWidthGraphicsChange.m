/* StepWidthGraphicsChange.m
 *
 * Copyright (C) 2002 by Cenon GmbH
 * Authors:  Ilonka Fleischmann
 *
 * created:  2002-07-15
 * modified: 2002-07-15
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include "undo.h"

@interface StepWidthGraphicsChange(PrivateMethods)

- (BOOL)subsumeIdenticalChange:change;

@end

@implementation StepWidthGraphicsChange

- initGraphicView:aGraphicView stepWidth:(float)sWidth
{
    [super initGraphicView:aGraphicView];
    widthValue = sWidth;
    return self;
}

- (NSString *)changeName
{
    return STEPWIDTH_OP;
}

- (Class)changeDetailClass
{
    return [StepWidthChangeDetail class];
}

- (float)stepWidth
{
    return widthValue;
}

- (BOOL)subsumeIdenticalChange:change
{
//    widthValue = [(WidthGraphicsChange *)change stepWidth];
    return NO;
}

@end
