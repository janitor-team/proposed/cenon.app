/* EndEditingGraphicsChange.h
 * This change is created when the user finishes editing a text
 * graphic. Undoing this change re-inserts the text editing
 * cursor in the text. More significantly, undoing this change
 * swaps the contents of the VText back into the field editor
 * so that it is ready to edit.
 *
 * Copyright (C) 1993-2014 by Cenon GmbH
 * Authors:  Georg Fleischmann
 *
 * created:  1993
 * modified: 2002-07-15
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include "../Graphics.h"

@interface EndEditingGraphicsChange: Change
{
    id 		graphicView;
    VText	*graphic;
    NSData	*oldData, *newData;
    NSRect	oldBounds, newBounds;
}

- (id)initGraphicView:(id)aGraphicView graphic:(VText*)aGraphic;
- (NSString *)changeName;
- (void)undoChange;
- (void)redoChange;

@end
