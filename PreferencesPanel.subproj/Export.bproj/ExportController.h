/* ExportController.h
 * Preferences module for Cenon Exports
 *
 * Copyright (C) 2002 by Cenon GmbH
 * Author: Georg Fleischmann
 *
 * Created:  2002-07-01
 * Modified: 2002-07-07
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_PREFEXPORTCONTROLLER
#define CEN_H_PREFEXPORTCONTROLLER

#include <AppKit/AppKit.h>
#include "../PreferencesMethods.h"

/* index of column for switches in switchMatrix */
#define SWITCH_FLATTENTEXT	0

@interface ExportController:NSObject <PreferencesMethods>
{
    id box;

    id switchMatrix;
}

- (void)set:sender;

@end

#endif // CEN_H_PREFEXPORTCONTROLLER
