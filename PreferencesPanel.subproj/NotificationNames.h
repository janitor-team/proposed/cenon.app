/* NotificationNames.h
 * Common names of notification related to preferences
 *
 * Copyright (C) 2002-2015 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * Created:  2002-06-30
 * Modified: 2015-11-25 (PrefsExpertModeHasChanged added)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#define PrefsCachingHasChanged      @"PrefsCachingHasChanged"       // all documents
#define PrefsUnitHasChanged         @"PrefsUnitHasChanged"          // unit has changed (also used for Document)

#define PrefsAllLayersHaveChanged   @"PrefsAllLayersHaveChanged"    // set all layers dirty

#define PrefsExpertModeHasChanged   @"PrefsExpertModeHasChanged"    // set expertMode for xyz
