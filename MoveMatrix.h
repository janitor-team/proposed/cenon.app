/* MoveMatrix.h
 *
 * Copyright (C) 1993-2015 by Cenon GmbH
 * Author: T+T Hennerich, Georg Fleischmann
 *
 * Created:  1993-05-17
 * Modified: 2015-11-30 (-matrixShouldShuffleCells for not shuffeling cells added)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_MOVEMATRIX
#define CEN_H_MOVEMATRIX

@interface MoveMatrix: NSMatrix
{
    NSImage	*matrixCache;
    NSImage	*cellCache;
    id  	activeCell;	// Cell currently move using the Control key

    id  	delegate;
}

- (id)initCellClass:aCellClass;
- (void)calcCellSize;

- (NSCell*)makeCellAtRow:(int)row column:(int)col;

- (void)mouseDown:(NSEvent *)theEvent;
- (void)shuffleCell:(int)row to:(int)newRow;
- (void)drawRect:(NSRect)rect;

- (void)setupCache;
- (NSImage*)sizeCache:(NSImage*)cacheWindow to:(NSSize)windowSize;

- (void)setDelegate:(id)anObject;
- (id)delegate;

- (void)dealloc;

@end


@interface PossibleDelegate: NSObject
{}

- (BOOL)matrixShouldShuffleCells;
- (void)matrixDidShuffleCellFrom:(int)row to:(int)newRow;

@end

#endif // CEN_H_MOVEMATRIX
