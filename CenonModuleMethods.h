/* CenonModuleMethods.h
 * Common methods used in preferences bundles
 *
 * Copyright (C) 2003-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * Created:  2003-04-09
 * Modified: 2013-12-19 (terminate)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.cenon.de
 */

#ifndef CEN_H_CENONMODULEMETHODS
#define CEN_H_CENONMODULEMETHODS

#include <AppKit/AppKit.h>

@protocol CenonModuleMethods

+ (id)instance;
- (id)init;

- (NSString*)version;
- (NSString*)compileDate;   // compile date or nil
- (NSString*)serialNo;      // serial number or nil
- (NSString*)netId;         // origin

- (void)terminate;

@end

#endif	// CEN_H_CENONMODULEMETHODS
