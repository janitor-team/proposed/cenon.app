/* TileObject.m
 * Object managing batch production
 *
 * Copyright (C) 1996-2014 by Cenon GmbH
 * Author: Georg Fleischmann
 *
 * Created:  1996-05-10
 * Modified: 2002-07-15
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_TILEOBJECT
#define CEN_H_TILEOBJECT

#include <Foundation/Foundation.h>

@interface TileObject:NSObject
{
    NSPoint	position;
    float	angle;
}

- init;

- (void)setPosition:(NSPoint)p;
- (NSPoint)position;

- (void)setAngle:(float)a;
- (float)angle;

- (void)dealloc;

- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

@end

#endif // CEN_H_TILEOBJECT
