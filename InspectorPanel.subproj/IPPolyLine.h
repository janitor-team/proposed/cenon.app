/* IPPolyLine.h
 * PolyLine Inspector
 *
 * Copyright (C) 1995-2014 by Cenon GmbH
 * Author:   Ilonka Fleischmann
 *
 * created:  2001-08-30
 * modified: 2002-07-20
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPPOLYLINE
#define CEN_H_IPPOLYLINE

#include <AppKit/AppKit.h>
#include "IPBasicLevel.h"

@interface IPPolyLine:IPBasicLevel
{
    id	xField;
    id	yField;
    id	xButtonLeft;
    id	xButtonRight;
    id	yButtonLeft;
    id	yButtonRight;
}

- (void)update:sender;

- (void)setPointX:sender;
- (void)setPointY:sender;

- (void)displayWillEnd;

@end

#endif // CEN_H_IPPOLYLINE
