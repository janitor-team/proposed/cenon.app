/* IPCurve.h
 * Curve inspector
 *
 * Copyright (C) 1995-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  1995-12-09
 * modified: 2006-12-08
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPCURVE
#define CEN_H_IPCURVE

#include <AppKit/AppKit.h>
#include "IPBasicLevel.h"

@interface IPCurve:IPBasicLevel
{
    id		xField;
    id		yField;
    id		xButtonLeft;
    id		xButtonRight;
    id		yButtonLeft;
    id		yButtonRight;

    id		xc1Field;
    id		yc1Field;
    id		xc1ButtonLeft;
    id		xc1ButtonRight;
    id		yc1ButtonLeft;
    id		yc1ButtonRight;

    id		xc2Field;
    id		yc2Field;

    id		xeField;
    id		yeField;

    VGraphic	*graphic;
}

- (void)update:sender;

- (void)setPointX:sender;
- (void)setPointY:sender;
- (void)setControlX:sender;
- (void)setControlY:sender;

- (void)displayWillEnd;

@end

#endif // CEN_H_IPCURVE
