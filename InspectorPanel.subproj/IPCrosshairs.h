/* IPCrosshairs.h
 * Crosshair inspector
 *
 * Copyright (C) 1997-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  1995-12-09
 * modified: 2002-07-20
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPCROSSHAIRS
#define CEN_H_IPCROSSHAIRS

#include <AppKit/AppKit.h>
#include "IPBasicLevel.h"

@interface IPCrosshairs:IPBasicLevel
{
    id	xField;
    id	yField;
}

- (void)update:sender;

- (void)setPointX:sender;
- (void)setPointY:sender;

- (void)displayWillEnd;

@end

#endif // CEN_H_IPCROSSHAIRS
