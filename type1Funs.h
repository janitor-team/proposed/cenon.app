/* fontFuns.h
 * Functions helping with type 1 fonts
 *
 * Copyright (C) 1995-2014 by Cenon GmbH
 * Author: Georg Fleischmann
 *
 * Created:  1995-07-30
 * Modified: 2002-07-07
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_TYPE1FUNS
#define CEN_H_TYPE1FUNS

/* for interpreter
 */
#define SEPARATORS	" \t\n\r"
#define NEWLINE		"\n\r"

typedef unsigned char Proc;

unsigned char encryptByte(unsigned char plain);
unsigned char decryptByte(unsigned char cipher);
unsigned char *decryptCharString(unsigned char *string, int len);
unsigned char *encryptCharString(unsigned char *string, int len);
unsigned char *encryptEexec(unsigned char *string, int len);
unsigned char *decryptEexec(const unsigned char *string, int srcLen, int *desLen);
unsigned char *decodeNumber(unsigned char *str, int *value);
int encodeNumber(int value, unsigned char *charString);
char *getName(const char *cp);
char *getString(const char *cp);
int getInt(const char *cp);
BOOL getBool(const char *cp);
char *getArray(char *cp, float array[]);
char *getOtherSubrs(char *cp);

#endif // CEN_H_TYPE1FUNS
