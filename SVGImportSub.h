/* SVGImportSub.m
 * Subclass of SVG import
 *
 * Copyright (C) 2010 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  2010-07-04
 * modified: 2010-07-04
 *
 * This file is part of the Cenon Import Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by the Cenon GmbH. Among other things,
 * the License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this library; see the file LICENSE. If not, write to Cenon.
 *
 * If you want to link this library to your proprietary software,
 * or for other uses which are not covered by the definitions
 * laid down in the Cenon Public License, vhf also offers a proprietary
 * license scheme. See the vhf internet pages or ask for details.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_SVGIMPORTSUB
#define CEN_H_SVGIMPORTSUB

#include <AppKit/AppKit.h>
#include <VHFImport/SVGImport.h>

@interface SVGImportSub:SVGImport
{
}

/* subclassed from super class
 */
/*- allocateList;
- (void)addLine:(NSPoint)beg :(NSPoint)end toList:aList;
- (void)addArc:(NSPoint)center :(NSPoint)start :(float)angle toList:aList;
- (void)addCurve:(NSPoint)p0 :(NSPoint)p1 :(NSPoint)p2 :(NSPoint)p3 toList:aList;
- (void)addText:(NSString*)text :(NSString*)font :(float)angle :(float)size :(float)ar at:(NSPoint)p toList:aList;
- (void)addGroupList:aList toList:bList;
- (void)addFillList:aList toList:bList;
- (void)setBounds:(NSRect)bounds;*/

@end

#endif // CEN_H_SVGIMPORTSUB
