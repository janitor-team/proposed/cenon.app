/* PSSettings.h
 * settings panel for project settings
 *
 * Copyright (C) 2000-2014 by Cenon GmbH
 * Author:   Ilonka Fleischmann, Georg Fleischmann
 *
 * Created:  2000-11-23
 * Modified: 2006-09-21
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include <AppKit/AppKit.h>
#include "../DocView.h"

typedef enum
{
    SWITCH_COORDS = 0	// settings switches
} SettingsSwitches;

@interface PSSettings:NSObject
{
    id	view;
    id	unitPopup;
    id	switches;

    DocView	*docView;
}

- (id)init;
- (void)update:(id)sender;

- (NSString*)name;
- (NSView*)view;

- (void)set:sender;

@end
