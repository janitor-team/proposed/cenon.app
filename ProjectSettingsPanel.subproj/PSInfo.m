/* PSInfo.m
 * Info panel for project settings
 *
 * Copyright (C) 2002-2014 by Cenon GmbH
 * Author: Georg Fleischmann
 *
 * Created:  2002-11-23
 * Modified: 2003-06-26
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include "ProjectSettings.h"
#include "PSInfo.h"
#include "../App.h"
#include "../Document.h"
//#include "../locations.h"
//#include "../messages.h"


@interface PSInfo(PrivateMethods)
@end

@implementation PSInfo

- (id)init
{
    [super init];

    if ( ![NSBundle loadNibNamed:@"PSInfo" owner:self] )
    {   NSLog(@"Cannot load 'PSInfo' interface file");
        return nil;
    }

    [self update:self];

    return self;
}

- (void)update:sender
{   id          doc = ([sender isKindOfClass:[Document class]]) ? sender : [(App*)NSApp currentDocument];
    NSString    *string;

    string = ([doc docVersion])   ? [doc docVersion]   : @"";
    [versionForm setStringValue:string];
    string = ([doc docAuthor])    ? [doc docAuthor]    : @"";
    [authorForm setStringValue:string];
    string = ([doc docCopyright]) ? [doc docCopyright] : @"";
    [copyrightForm setStringValue:string];
    string = ([doc docComment])   ? [doc docComment]   : @"";
    [commentText setString:string];
}

- (NSString*)name
{
    return [[view window] title];
}

- (NSView*)view
{
    return view;
}

- (void)set:sender
{   id		doc = ([sender isKindOfClass:[Document class]]) ? sender : [(App*)NSApp currentDocument];

    if (!doc)
        return;

    [doc setDocVersion:[versionForm stringValue]];
    [doc setDocAuthor:[authorForm stringValue]];
    [doc setDocCopyright:[copyrightForm stringValue]];
    [doc setDocComment:[commentText string]];
}


@end
