/*
 * TPBasicLevel.m
 *
 * Copyright (C) 1996-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * Created:  1996-03-03
 * Modified: 2003-06-26
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include "TPBasicLevel.h"

@implementation TPBasicLevel

- init
{
    [self setDelegate:self];
    return self;
}

- (void)setWindow:win
{
    window = win; 
}

- view
{
    return [[(App*)NSApp currentDocument] documentView];
}

- window
{
    return window;
}

- (void)update:sender
{
}

/* delegate methods
 */
- (void)displayWillEnd
{
}

@end
